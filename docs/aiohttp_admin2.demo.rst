aiohttp\_admin2.demo package
============================

Subpackages
-----------

.. toctree::

    aiohttp_admin2.demo.admin
    aiohttp_admin2.demo.auth
    aiohttp_admin2.demo.catalog

Submodules
----------

aiohttp\_admin2.demo.db module
------------------------------

.. automodule:: aiohttp_admin2.demo.db
    :members:
    :undoc-members:
    :show-inheritance:

aiohttp\_admin2.demo.routes module
----------------------------------

.. automodule:: aiohttp_admin2.demo.routes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aiohttp_admin2.demo
    :members:
    :undoc-members:
    :show-inheritance:
