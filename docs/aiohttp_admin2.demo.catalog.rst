aiohttp\_admin2.demo.catalog package
====================================

Submodules
----------

aiohttp\_admin2.demo.catalog.tables module
------------------------------------------

.. automodule:: aiohttp_admin2.demo.catalog.tables
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aiohttp_admin2.demo.catalog
    :members:
    :undoc-members:
    :show-inheritance:
