aiohttp\_admin2.demo.admin package
==================================

Subpackages
-----------

.. toctree::

    aiohttp_admin2.demo.admin.actors
    aiohttp_admin2.demo.admin.genres
    aiohttp_admin2.demo.admin.movies
    aiohttp_admin2.demo.admin.shows
    aiohttp_admin2.demo.admin.users

Module contents
---------------

.. automodule:: aiohttp_admin2.demo.admin
    :members:
    :undoc-members:
    :show-inheritance:
