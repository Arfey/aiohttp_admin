aiohttp\_admin2.demo.admin.shows package
========================================

Submodules
----------

aiohttp\_admin2.demo.admin.shows.controllers module
---------------------------------------------------

.. automodule:: aiohttp_admin2.demo.admin.shows.controllers
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aiohttp_admin2.demo.admin.shows
    :members:
    :undoc-members:
    :show-inheritance:
