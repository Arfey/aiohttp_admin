aiohttp\_admin2.demo.admin.genres package
=========================================

Submodules
----------

aiohttp\_admin2.demo.admin.genres.controllers module
----------------------------------------------------

.. automodule:: aiohttp_admin2.demo.admin.genres.controllers
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aiohttp_admin2.demo.admin.genres
    :members:
    :undoc-members:
    :show-inheritance:
