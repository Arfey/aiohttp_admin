aiohttp\_admin2.demo.admin.actors package
=========================================

Submodules
----------

aiohttp\_admin2.demo.admin.actors.controllers module
----------------------------------------------------

.. automodule:: aiohttp_admin2.demo.admin.actors.controllers
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aiohttp_admin2.demo.admin.actors
    :members:
    :undoc-members:
    :show-inheritance:
