aiohttp\_admin2.demo.admin.movies package
=========================================

Submodules
----------

aiohttp\_admin2.demo.admin.movies.controllers module
----------------------------------------------------

.. automodule:: aiohttp_admin2.demo.admin.movies.controllers
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aiohttp_admin2.demo.admin.movies
    :members:
    :undoc-members:
    :show-inheritance:
