aiohttp\_admin2.views package
=============================

Subpackages
-----------

.. toctree::

    aiohttp_admin2.views.aiohttp
    aiohttp_admin2.views.filters
    aiohttp_admin2.views.widgets

Module contents
---------------

.. automodule:: aiohttp_admin2.views
    :members:
    :undoc-members:
    :show-inheritance:
